# GeoJson for Solar Eclipse 8 April 2024
`path.json` gives the northern and southern limits of totality for this eclipse, at 2 minute intervals.
Points along the centerline of this area give the time (UTC) and duration of totality.

The eclipse table in `limits.txt` is sourced from [NASA](https://eclipse.gsfc.nasa.gov/SEpath/SEpath2001/SE2024Apr08Tpath.html):
**Eclipse table courtesy of Fred Espenak, NASA/Goddard Space Flight Center, from eclipse.gsfc.nasa.gov.**

`makepath.sh` converts `limits.txt` to `path.json`.
It is messy. 
I would consider it an example of how *not* to accomplish this.
The output is intended for import to CalTopo, and I have not tested it beyond that.

This work is public domain. 
[CC0](https://creativecommons.org/publicdomain/zero/1.0/)
