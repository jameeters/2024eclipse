#! /bin/bash

echo '{"type": "FeatureCollection",'
echo '	"features": ['
echo '		{'
echo '			"type": "Feature",'
echo '			"properties": {"title": "Northern Limit"},'
echo '			"geometry": {'
echo '				"type": "LineString",'
echo '				"coordinates": ['
tail +9 limits.txt \
	| grep . \
	| grep -v -E '(Limits|19:54)' \
	| grep -v 'S' \
	| sed 's/\(N\|W\)//g' \
	| awk '{print  "[-"$4 + ($5/60)", "$2 + ($3/60)"]," }' \
	| sed '$s/,$//' \
	| awk '{print "					"$0}' 
echo '				]'
echo '			}'
echo '		},'

echo '		{'
echo '			"type": "Feature",'
echo '			"properties": {"title": "Southern Limit"},'
echo '			"geometry": {'
echo '				"type": "LineString",'
echo '				"coordinates": ['
tail +9 limits.txt \
	| grep . \
	| grep -v -E '(Limits|19:54)' \
	| grep -v 'S' \
	| sed 's/\(N\|W\)//g' \
	| awk '{print  "[-"$8 + ($9/60)", "$6 + ($7/60)"]," }' \
	| sed '$s/,$//' \
	| awk '{print "					"$0}' 
echo '				]'
echo '			}'
echo '		},'

tail +9 limits.txt \
	| grep . \
	| grep -v -E '(Limits|19:54)' \
	| grep -v 'S' \
	| sed 's/\(N\|W\)//g' \
	| awk '{print "{\"type\": \"Feature\", \"properties\":{\"title\":\"" $1 " (" $18 ")\"}, \"geometry\": {\"type\": \"Point\", \"coordinates\": [-" $12 + ($13/60) ", " $10 + ($11/60) "]} }," }' \
	| sed '$s/,$//' \
	| awk '{print "		"$0}' 


echo '	]'
echo '}'
